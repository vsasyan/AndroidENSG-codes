package fr.ign.camerax;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.google.common.util.concurrent.ListenableFuture;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {
    private ImageCapture imageCapture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Vérification de l'autorisation d'accès la camera
        if (checkSelfPermission(android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityResultLauncher<String> requestPermissionLauncher =
                    registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
                        if (!isGranted) {
                            Toast.makeText(MainActivity.this, getString(R.string.permission_needed), Toast.LENGTH_LONG).show();
                            Log.i("ENSG", "permission denied");
                        } else {
                            startCamera();
                        }
                    });
            requestPermissionLauncher.launch(Manifest.permission.CAMERA);
        } else {
            startCamera();
        }

        // Déclaration, instantiation et usage des composants graphiques
        Button b_takePicture = findViewById(R.id.image_capture_button);
        b_takePicture.setOnClickListener(view -> {
            takePhoto();
        });
    }

    private void startCamera() {
        Log.i("ENSG", "startCamera");

        ListenableFuture<ProcessCameraProvider> cameraProviderFuture  = ProcessCameraProvider.getInstance(this);
        cameraProviderFuture.addListener(new Runnable() {
            @Override
            public void run() {
                // Used to bind the lifecycle of cameras to the lifecycle owner
                ProcessCameraProvider cameraProvider = null;
                try {
                    cameraProvider = cameraProviderFuture.get();

                    // Prévisualisation
                    PreviewView previewView = findViewById(R.id.viewFinder);
                    Preview preview = new Preview.Builder().build();
                    preview.setSurfaceProvider(previewView.getSurfaceProvider());

                    // Récupération de l'image
                    imageCapture = new ImageCapture.Builder().build();

                    // On choisi l'appareil au dos TODO : laisser le choix à l'utilisateur !
                    CameraSelector cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA;

                    // On coupe les liens déjà existants si jamais
                    cameraProvider.unbindAll();
                    // On crée le lien avec notre previsualisation
                    cameraProvider.bindToLifecycle(MainActivity.this, cameraSelector, preview, imageCapture);
                } catch (ExecutionException e) {
                    Log.e("ENSG", "ExecutionException failure", e);
                } catch (InterruptedException e) {
                    Log.e("ENSG", "InterruptedException failure", e);
                } catch (Exception e) {
                    Log.e("ENSG", "Use case binding failed", e);
                }
            }
        }, getMainExecutor());
    }

    private void takePhoto() {
        // On vérifie que imageCapture est instancié
        if (imageCapture != null) {
            // Récupération d'une entrée MediaStore nommée selon la date et l'heure
            // 1) On converti la date en nom de fichier
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS", Locale.US);
            String name = String.format("%s.jpeg", dateFormatter.format(System.currentTimeMillis()));
            // 2) On demande la création d'une entrée MediaStore pour notre image
            ContentValues contentValues = new ContentValues();
            // 2-a) On donne son nom
            contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, name);
            // 2-a) On donne son type
            contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");
            // 2-c) Résolution de l'entrée à partir des info données
            ContentResolver contentResolver = getApplicationContext().getContentResolver();
            ImageCapture.OutputFileOptions outputFileOptions = new ImageCapture.OutputFileOptions
                    .Builder(contentResolver, MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)
                    .build();

            // On utilise le imageCapture pour prendre la photo (encore un système de callback s/e)
            imageCapture.takePicture(outputFileOptions, ContextCompat.getMainExecutor(this), new ImageCapture.OnImageSavedCallback() {
                @Override
                public void onImageSaved(@NonNull ImageCapture.OutputFileResults outputFileResults) {
                    // La photo a été prise et enregistrée
                    String msg = String.format("Photo capture succeeded: %s", name);
                    Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                    Log.d("ENSG", msg);
                }

                @Override
                public void onError(@NonNull ImageCaptureException exception) {
                    // Erreur
                    String msg = String.format("Photo capture failed: %s", exception.getMessage());
                    Log.e("ENSG", msg, exception);
                }
            });
        } else {
            Log.e("ENSG", "imageCapture is null");
        }
    }
}