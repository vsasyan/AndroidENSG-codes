package fr.ign.twosidesofthecoin;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    // On déclare une constante qui servira de "clé"
    static final String TEXT_VIEW_CONTENT = "TEXT_VIEW_CONTENT";

    // Déclaration des attributs de classe pour stocker les instances Java des composants graphiques
    TextView tv_coinResult;
    Button b_flip_coin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i("ENSG", "onCreate");

        // Instanciation des composants graphiques
        tv_coinResult = findViewById(R.id.tv_coin_result);
        b_flip_coin = findViewById(R.id.b_flip_coin);

        // Ajout de l'écouteur d'événement
        b_flip_coin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // On appelle la méthode pour lancer la pièce
                coinFlip();
            }
        });

        // On appelle coinFlip
        coinFlip();
    }

    protected void coinFlip() {
        // Génération aléatoire d'un booléen en Java
        Random randomGenerator = new Random();
        boolean random = randomGenerator.nextBoolean();

        // Récupération des string dans les ressources
        String tail = getString(R.string.tail);
        String head = getString(R.string.head);

        // Définition de la valeur selon le résultat
        String result = tail;
        if (random) {
            result = head;
        }

        // Récupération du contenu
        String previousContent = tv_coinResult.getText().toString();
        if (!previousContent.equals("")) {
            result = previousContent + '\n' + result;
        }

        // Modification du composant graphique
        tv_coinResult.setText(result);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // On ajout à l'objet Bundle le contenu du TextView, associé à la clef
        outState.putString(TEXT_VIEW_CONTENT, tv_coinResult.getText().toString());

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        // On récupère de l'objet Bundle la valeur associée à la clef
        String savedValue = savedInstanceState.getString(TEXT_VIEW_CONTENT);
        // On défini le contenu du TextView
        tv_coinResult.setText(savedValue);

        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("ENSG", "onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("ENSG", "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("ENSG", "onDestroy");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("ENSG", "onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("ENSG", "onResume");
    }
}