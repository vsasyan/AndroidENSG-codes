package fr.ign.positionname;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.Priority;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import fr.ign.positionname.databinding.ActivityMapsBinding;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationClient;
    public Marker currentLocationMarker;

    private final ResultReceiver currentLocationResultReceiver = new ResultReceiver(new Handler()) {
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            // Is success?
            if (resultCode == FetchAddressIntentService.CODE_SUCCESS) {
                // Yes (result is address)
                String result = resultData.getString(FetchAddressIntentService.RESULT_ADDRESS);
                setCurrentLocationMarkerTitle(result);
                Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
            } else {
                // No (result is error message)
                String message = resultData.getString(FetchAddressIntentService.RESULT_MESSAGE);
                setCurrentLocationMarkerTitle("");
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityMapsBinding binding = ActivityMapsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Demande la permission d'accès à la position
        ActivityResultLauncher<String[]> locationPermissionRequest =
                registerForActivityResult(new ActivityResultContracts
                                .RequestMultiplePermissions(), result -> {
                            Boolean fineLocationGranted = result.getOrDefault(
                                    android.Manifest.permission.ACCESS_FINE_LOCATION, false);
                            Boolean coarseLocationGranted = result.getOrDefault(
                                    android.Manifest.permission.ACCESS_COARSE_LOCATION,false);
                            if (fineLocationGranted != null && fineLocationGranted) {
                                // Precise location access granted.
                                Log.i("ENSG", "fineLocationGranted");
                                getLastLocation();
                                getCurrentLocation();
                            } else if (coarseLocationGranted != null && coarseLocationGranted) {
                                // Only approximate location access granted.
                                Log.i("ENSG", "coarseLocationGranted");
                                getLastLocation();
                                getCurrentLocation();
                            } else {
                                // No location access granted.
                                Log.e("ENSG", "noLocationGranted");
                            }
                        }
                );
        locationPermissionRequest.launch(new String[] {
                android.Manifest.permission.ACCESS_FINE_LOCATION,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
        });

        // Instanciation du client de position
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
    }

    protected void getLastLocation() throws SecurityException {
        mFusedLocationClient.getLastLocation()
                // Success
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // On a eu la dernière position, on vérifie qu'elle n'est pas nulle
                        if (location != null) {
                            Log.i("ENSG", "Position:" + location);
                            addLastLocationMarker(location);
                        } else {
                            Log.i("ENSG", "Position nulle...");
                        }
                    }
                })
                // Failure
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("ENSG", "Failure...");
                        e.printStackTrace();
                    }
                })
        ;
    }

    private void addLastLocationMarker(Location location) {
        LatLng position = new LatLng(location.getLatitude(), location.getLongitude());
        if (mMap != null) {
            mMap.addMarker(new MarkerOptions().position(position).title("lastLocation"));
        } else {
            Log.w("ENSG", "Carte non initialisée. Impossible de mettre le lastLocation marker à " + location);
        }
    }

    protected void getCurrentLocation() throws SecurityException {
        // Définition des paramètres de notre requête de mise à jour de la position
        LocationRequest locationRequest = new LocationRequest.Builder(Priority.PRIORITY_HIGH_ACCURACY, 5000)
                .setWaitForAccurateLocation(false)
                .setMinUpdateIntervalMillis(2000)
                .setMaxUpdateDelayMillis(10000)
                .build();
        // Callback de mise à jour
        LocationCallback locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                for (Location location : locationResult.getLocations()) {
                    Log.i("ENSG", "currentLocation : " + location);
                    setCurrentLocationMarkerLocation(location);
                }
            };
        };
        // Lancement de la demande
        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
    }

    public void setCurrentLocationMarkerLocation(Location location) {
        // Est-ce que la carte est ok ?
        if (mMap != null) {
            LatLng position = new LatLng(location.getLatitude(), location.getLongitude());
            // Faut-il créer le marker ou juste le déplacer ?
            if (currentLocationMarker == null) {
                // Création
                currentLocationMarker = mMap.addMarker(new MarkerOptions().position(position).title("Current location"));
            } else {
                // Déplacement
                currentLocationMarker.setPosition(position);
            }
            // Geocodage
            geocode(location);
        } else {
            Log.w("ENSG", "Carte non initialisée. Impossible de mettre le currentLocation marker à " + location);
        }
    }
    public void geocode(Location location) {
        FetchAddressIntentService.startActionGeocode(this, location, currentLocationResultReceiver);
    }

    public void setCurrentLocationMarkerTitle(String title) {
        if (currentLocationMarker != null) {
            currentLocationMarker.setTitle(title);
        } else {
            Log.w("ENSG", "Marker non initialisé. Impossible de mettre l'adresse à jour : " + title);
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng ensg = new LatLng(48.8410837, 2.5875354);
        mMap.addMarker(new MarkerOptions().position(ensg).title("ENSG"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ensg, 14));

        // Affichage outils de zoom
        mMap.getUiSettings().setZoomControlsEnabled(true);
    }
}