package fr.ign.rollingball;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.WindowCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.core.view.WindowInsetsControllerCompat;

import android.os.Build;
import android.os.Bundle;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowInsets;
import android.view.WindowInsetsController;
import android.view.WindowManager;

public class MainActivity extends AppCompatActivity {
    private PhysicalEngine physicalEngine;
    private GraphicalEngine graphicalEngine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Instanciation
        Ball ball = new Ball();
        physicalEngine = new PhysicalEngine(this, ball);
        graphicalEngine = new GraphicalEngine(ball);
        SurfaceView surfaceView = findViewById(R.id.surfaceView);
        // Lien moteur graphique / SurfaceView
        surfaceView.getHolder().addCallback(graphicalEngine);

        // Passage en plein écran
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            final WindowInsetsController insetsController = getWindow().getInsetsController();
            if (insetsController != null) {
                insetsController.hide(WindowInsets.Type.statusBars());
            }
        } else {
            supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(
                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN
            );
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        physicalEngine.startEngine();
    }

    @Override
    protected void onStop() {
        physicalEngine.stopEngine();
        super.onStop();
    }
}