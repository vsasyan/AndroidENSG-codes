package fr.ign.rollingball;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

public class PhysicalEngine {
    private final SensorManager sensorManager;
    private final Sensor accelerometer;
    private final Ball ball;

    private final SensorEventListener sensorEventListener = new SensorEventListener() {
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        public void onSensorChanged(SensorEvent sensorEvent) {
            float accelerationX = -sensorEvent.values[0];
            float accelerationY = sensorEvent.values[1];
            moveBall(accelerationX, accelerationY);
        }
    };

    protected PhysicalEngine(Activity activity, Ball ball) {
        this.ball = ball;
        sensorManager = (SensorManager)activity.getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    private void moveBall(float accelerationX, float accelerationY) {
        Log.i("ENSG", String.format("moveBall %f %f", accelerationX, accelerationY));
        ball.acceleration(accelerationX, accelerationY);
        ball.move();
    }

    protected void startEngine() {
        sensorManager.registerListener(
                sensorEventListener,
                accelerometer,
                SensorManager.SENSOR_DELAY_GAME
        );
    }
    protected void stopEngine() {
        sensorManager.unregisterListener(sensorEventListener, accelerometer);
    }
}
