package fr.ign.rollingball;

public class Ball {
    public static final float RADIUS = 20;
    public static final float MAX_SPEED = 20;
    public static final float SPEED_FACTOR = 0.95f;
    public static final float ACCELERATION_FACTOR = 0.2f;

    private float maxX;
    private float maxY;
    private float x;
    private float y;
    private float speedX;
    private float speedY;

    public void acceleration(float accelerationX, float accelerationY) {
        // La vitesse est pondérée par SPEED_FACTORE et l'accélération par ACCELERATION_FACTOR
        speedX = speedX * SPEED_FACTOR + accelerationX * ACCELERATION_FACTOR;
        speedY = speedY * SPEED_FACTOR + accelerationY * ACCELERATION_FACTOR;
        // Si les vitesses sont trop importantes, on les réduit (en conservant leur signe)
        if (Math.abs(speedX) > MAX_SPEED) {
            speedX = (Math.abs(speedX) / speedX) * MAX_SPEED;
        }
        if (Math.abs(speedY) > MAX_SPEED) {
            speedY = (Math.abs(speedY) / speedY) * MAX_SPEED;
        }
    }

    public void move() {
        // On déplace
        x += speedX;
        y += speedY;
        // On vérifie la position
        checkPosition();
    }

    public void checkPosition() {
        // Si dépassement, on renvoie dans l'autre sens et on inverse la vitesse
        if (x >= maxX - RADIUS) {
            x = maxX - RADIUS;
            speedX = -speedX;
        }
        if (x <= 0 + RADIUS) {
            x = 0 + RADIUS;
            speedX = -speedX;
        }
        if (y >= maxY - RADIUS) {
            y = maxY - RADIUS;
            speedY = -speedY;
        }
        if (y <= 0 + RADIUS) {
            y = 0 + RADIUS;
            speedY = -speedY;
        }
    }

    public void setMaxX(float maxX) {
        // On set le max
        this.maxX = maxX;
        // Et on met la balle au milieu
        this.x = maxX / 2;
    }

    public void setMaxY(float maxY) {
        // On set le max
        this.maxY = maxY;
        // Et on met la balle au milieu
        this.y = maxY / 2;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }
}
